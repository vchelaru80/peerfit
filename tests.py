from hands import *

#player 1 rank is better
assert compare("QS QD AC AD 4C 6S 2D AS 3H KC") is True

#player 2 rank is better

assert compare("4C 7C 3C TD QS 9C KC AS 8D AD") is False

#test tie and the player 1 wins
assert compare("2H TC 8S AD 9S 4H TS 7H 2C 5C") is True

#test tie and the player 2 wins
assert compare("4H 2S 6C 5S KS AH 9C 7C 8H KD") is False


hand = "AD 6C 6S 7D TH 6H 2H 8H KH 4H"
cards = hand.split(" ")
p1 = cards[0:5]
p2 = cards[5:]
assert is_flush(p2)